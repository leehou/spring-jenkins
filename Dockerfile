FROM openjdk:11
RUN mkdir /app
WORKDIR /app

ADD ./target/jenkins-0.0.1-SNAPSHOT.jar /app/app.jar

EXPOSE 8081
ENTRYPOINT ["java", "-jar", "app.jar"]